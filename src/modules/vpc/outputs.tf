
output "private_subnets" {
  value = google_compute_subnetwork.private_subnets.*.id
}

output "vpc" {
  value = google_compute_network.main-vpc.id
}